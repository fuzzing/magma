#!/bin/bash

fuzzer=qsym

export UNIQ_JOB_ID=$(date +%s)

for target in \
libxml2_1 \
openssl_2 \
php_2 \
poppler_2
do 
	LAUNCH="nohup ./scripts/launch.sh --fuzzer ${fuzzer} -N 2 --limit $(( 86400 * 7 )) ${target} > ~/archive/logs/testing-${fuzzer}-${target}.log & "
	eval $LAUNCH
	jobs
	sleep 30s
done


## Full list of targets/benchmarks.
# libpng_0 \
# libtiff_0 \
# libtiff_1 \
# libxml2_0 \
# libxml2_1 \
# openssl_0 \
# openssl_1 \
# openssl_2 \
# openssl_3 \
# openssl_4 \
# openssl_5 \
# php_0 \
# php_1 \
# php_2 \
# php_3 \
# poppler_0 \
# poppler_1 \
# poppler_2 \
# sqlite3_0
