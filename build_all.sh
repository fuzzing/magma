#!/bin/bash -e

unset REGISTRY

export COMPOSE_DOCKER_CLI_BUILD=1 
export DOCKER_BUILDKIT=1 

if ! command -v docker-compose &>/dev/null ; then
   echo "[-] Error; please install docker-compose first."
   exit 1
fi

if [ ! -e docker-compose.yml ] || [ ! -e .env ]; then
    echo "[-] Error: please execute from the root repository directory."
    exit 1
fi

if [ $# -gt 0 ]; then
    echo "$@"
    docker-compose build $@ || exit 1
    docker-compose push $@
    exit 0
fi

# build the base image
docker-compose build base 

# build the fuzzers
docker-compose build afl aflplusplus aflplusplus_lto entropic honggfuzz  \
                     llvm_analysis symcc_afl vanilla || exit 1

# push to the repository
docker-compose push afl aflplusplus aflplusplus_lto entropic honggfuzz llvm_analysis symcc_afl vanilla
