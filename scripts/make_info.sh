#!/bin/bash

magma_root="benchmarks"

printf "challenges:\n"

i=1
for d in ${magma_root}/*/vanilla/bin/*; do
	bm=$(basename $(dirname ${d/vanilla\/bin/}));
	ft="$(basename ${d})";
	printf "    ${bm}:\n"
	printf "        challenge_id: $i\n"
	printf '        architecture: "x86_64"\n'
	printf "        install_dir: \"${bm//./_}\"\n"
	printf "        binary_path: \"${ft}\"\n\n"
	i="$(( $i + 1 ))"
done


