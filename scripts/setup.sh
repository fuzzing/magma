#!/bin/bash -eu

create_job_configs() {
	python3 scripts/create-configs.py -e example_job.json -y benchmarks/info.yaml -j AFL -c afl_job.json -p afl -M 0 -F >/dev/null
	python3 scripts/create-configs.py -e example_job.json -y benchmarks/info.yaml -j AFL -c job.json -M 0 -F >/dev/null
	python3 scripts/create-configs.py -e example_job.json -y benchmarks/info.yaml -j QSYM -c qsym_job.json -p afl -M 0 -F --merge scripts/templates/qysm_job.json >/dev/null
}

extract_corpus_libpng() {
    rm -rf benchmarks/libpng_0/seeds
    tar -C benchmarks/libpng -xf  benchmarks/libpng/corpus.tar.gz
    mv benchmarks/libpng/corpus/libpng_read_fuzzer benchmarks/libpng_0/seeds
    rmdir benchmarks/libpng/corpus
}

extract_corpus_libtiff() {
   tar -C benchmarks/libtiff -xf benchmarks/libtiff/corpus.tar.gz
   mv benchmarks/libtiff/corpus/tiff_read_rgba_fuzzer benchmarks/libtiff_0/seeds
   mv benchmarks/libtiff/corpus/tiffcp benchmarks/libtiff_1/seeds
   rmdir benchmarks/libtiff/corpus
}

extract_corpus_sqlite3() {
    tar -C benchmarks/sqlite3_0 -xf benchmarks/sqlite3/corpus.tar.gz
    mv benchmarks/sqlite3_0/corpus/sqlite3_fuzz benchmarks/sqlite3_0/seeds
    rmdir benchmarks/sqlite3_0/corpus
}



download_afl_binaries() {
	local gitref=${GITREF:-v2021-01-01}
        local tmpfile="/tmp/magma.zip"
        rm -f $tmpfile
	wget -qO $tmpfile "https://gitlab.com/fuzzing/magma/-/jobs/artifacts/${gitref}/download?job=deploy:afl"
	python3 -m zipfile -e $tmpfile .
	chmod +x benchmarks/*/afl/bin/*
}

download_binaries() {
	local gitref=${GITREF:-v2021-01-01}
        local jobname="${1:-vanilla}"
        local tmpfile="/tmp/magma.zip"
        rm -f $tmpfile
	wget -qO $tmpfile "https://gitlab.com/fuzzing/magma/-/jobs/artifacts/${gitref}/download?job=deploy:${jobname}"
	python3 -m zipfile -e $tmpfile .
	chmod +x benchmarks/*/${jobname}/bin/*
}


download_extra_dicts() {
    wget -qO benchmarks/libpng_0/png.dict 'https://raw.githubusercontent.com/google/AFL/master/dictionaries/png.dict'
    wget -qO benchmarks/libtiff_0/tiff.dict 'https://raw.githubusercontent.com/google/AFL/master/dictionaries/tiff.dict'
    ln -f benchmarks/libtiff_0/tiff.dict benchmarks/libtiff_1/
    wget -qO benchmarks/libxml2_0/xml.dict 'https://raw.githubusercontent.com/google/AFL/master/dictionaries/xml.dict'
    ln -f benchmarks/libxml2_0/xml.dict benchmarks/libxml2_1/xml.dict
    wget -qO benchmarks/poppler_0/pdf.dict 'https://raw.githubusercontent.com/google/fuzzing/master/dictionaries/pdf.dict'
    wget -qO benchmarks/sqlite3_0/sql.dict 'https://raw.githubusercontent.com/google/fuzzing/master/dictionaries/sql.dict'
}

export GITREF=master
# create_job_configs
# download_binaries "vanilla"
download_afl_binaries
# download_extra_dicts
