#!/bin/bash

cp ${TARGET}/configrc ${BK_ROOT}/

cd ${OUT}
ls -l

for p in ${!PROGRAMS[@]}; do 
	prog="${PROGRAMS[$p]}"
	mkdir -p ${BK_ROOT}_${p}/${FUZZER_NAME}/bin
	test -e ${prog} && cp ${prog} ${BK_ROOT}_${p}/${FUZZER_NAME}/bin/
done


for p in ${!PROGRAMS[@]}; do 
	export PROGRAM="${PROGRAMS[$p]}"
        cd ${TARGET}/corpus
	${CI_PROJECT_DIR}/scripts/prune_seeds.sh
	mv ${PROGRAM} seeds 
	tar -czf ${BK_ROOT}_${p}/corpus.tar.gz seeds
	rm -rf seeds
done
