[![pipeline status](https://gitlab.com/fuzzing/magma/badges/master/pipeline.svg)](https://gitlab.com/fuzzing/magma/-/commits/master)
# magma

Extracted fuzzing benchmarks from [Magma](https://hexhive.epfl.ch/magma)
